#!/bin/bash

set -e

readonly board_name="fuego-test"
readonly test_name="Functional.fuegotest"

if [ -e /fuego-ro/conf/fuego.conf ] ; then
    . /fuego-ro/conf/fuego.conf
else
    echo "ERROR: Missing fuego-ro/conf/fuego.conf"
fi

function wait_for_jenkins {
    readonly jenkins_port=${JENKINS_PORT:-8080}
    readonly jenkins_url="http://localhost:${jenkins_port}/fuego/"

    echo "Waiting for Jenkins..."
    while true;
    do
        sleep 5
        local http_code=$(curl -w "%{http_code}" --output /dev/null --silent --head --fail "${jenkins_url}")
        # Wait until the HTTP 200 is returned which means jenkins is ready.
        # Jenkins returns 503 while it's setting up.
        if [ "$http_code" == "200" ]; then
            break
        fi
    done
}

function node_exists {
    node_name="${1}"
    ftc list-nodes -q | grep -q "${node_name}"
    return "${?}"
}

function job_exists {
    job_name="${1}"
    node_name="${2}"
    ftc list-jobs -q | grep "${node_name}" | grep -q "${job_name}"
    return "${?}"
}

test_repo=${TEST_REPO:-https://bitbucket.org/profusionmobi/fuego}
test_repo_escaped=$(sed 's/[&/\]/\\&/g' <<< "${test_repo}")
sed -i "s/FUEGOTEST_REPO/${test_repo_escaped}/" /fuego-core/engine/tests/Functional.fuegotest/spec.json

test_branch=${TEST_BRANCH:-"fuego-base"}
test_branch_escaped=$(sed 's/[&/\]/\\&/g' <<< "${test_branch}")
sed -i "s/FUEGOTEST_BRANCH/${test_branch_escaped}/" /fuego-core/engine/tests/Functional.fuegotest/spec.json

wait_for_jenkins

if ! node_exists "${board_name}"; then
    ftc add-nodes "${board_name}"
fi

if ! job_exists "${test_name}"; then
    ftc add-jobs -b ${board_name} -t ${test_name} Functional.fuegotest
fi

exec "${@}"
