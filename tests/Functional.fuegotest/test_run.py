#!/usr/bin/env python3
import argparse
import http
import http.client
import logging
import os
import re
import subprocess
import sys
import time

import docker
import pexpect

LOGGER = logging.getLogger('test_run')
STREAM_HANDLER = logging.StreamHandler()
STREAM_HANDLER.setFormatter(logging.Formatter('%(name)s:%(levelname)s: %(message)s'))
LOGGER.setLevel(logging.DEBUG)
LOGGER.addHandler(STREAM_HANDLER)


class Command:
    BASH_PATTERN = 'test_run_pr1:#'
    COMMAND_RESULT_PATTERN = re.compile('^([0-9]+)', re.M)
    OUTPUT_VARIABLE = 'cmd_output'
    COMMAND_OUTPUT_DELIM = ':test_run_cmd_out:'
    COMMAND_OUTPUT_PATTERN = re.compile(r'^{0}(.*){0}\s+{1}'.format(COMMAND_OUTPUT_DELIM,
                                                                    BASH_PATTERN), re.M | re.S)

    def __init__(self, cmd, expected_output=None, expected_result=0):
        self.cmd = cmd
        self.expected_result = expected_result
        self.expected_output = expected_output

    def execute(self, client):
        LOGGER.debug('Executing command \'%s\'', self.cmd)
        try:
            client.sendline('{}=$({} 2>&1)'.format(Command.OUTPUT_VARIABLE, self.cmd))
            client.expect(Command.BASH_PATTERN)

            client.sendline('echo $?')
            client.expect(Command.COMMAND_RESULT_PATTERN)
            result = int(client.match.group(1))

            client.sendline('echo "{0}${{{1}}}{0}"'.format(Command.COMMAND_OUTPUT_DELIM,
                                                           Command.OUTPUT_VARIABLE))
            client.expect(Command.COMMAND_OUTPUT_PATTERN)
            out = client.match.group(1)

            if result != self.expected_result:
                LOGGER.error('The command \'%s\' returned the code \'%d\', ' \
                             'but the expected code is \'%d\'\nCommand output: \'%s\'',
                             self.cmd, result, self.expected_result, out)
                return False
            if self.expected_output is not None and re.search(self.expected_output, out) is None:
                LOGGER.error('Wrong output for command \'%s\'. Expected \'%s\'\nReceived \'%s\'',
                             self.cmd, self.expected_output, out)
                return False
        except pexpect.exceptions.TIMEOUT:
            LOGGER.error('Timeout for command \'%s\'', self.cmd)
            return False
        except pexpect.exceptions.EOF:
            LOGGER.error('Lost connection with docker. Aborting')
            return False
        return True

    @staticmethod
    def set_ps1(client):
        client.sendline('export PS1="{}"'.format(Command.BASH_PATTERN))
        client.expect(Command.BASH_PATTERN)

# This array contains the commands that should be tested
COMMANDS_TO_TEST = [
    Command('echo $\'hello\n\n\nfrom\n\n\ncontainer\'', r'hello\s+from\s+container'),
    Command('cat -ThisOptionDoesNotExists', expected_result=1),
    Command('ftc add-nodes docker'),
    Command('ftc list-nodes', r'Jenkins nodes in this system:\s*docker\s*.*'),
    Command('ftc add-jobs -b docker -p testplan_docker'),
    Command('ftc list-jobs', r'Jenkins jobs in this system:(\s*docker\..*)+')
]

def setup_docker(install_script, image_name, container_name):
    # Install the docker image.
    cmd = './{} {}'.format(install_script, image_name)
    LOGGER.debug('Running \'%s\' to install the docker image. This may take a while....', cmd)
    status = subprocess.call(cmd, shell=True)
    if status != 0:
        return None
    docker_client = docker.from_env()
    containers = docker_client.containers.list(all=True, filters={'name': container_name})
    if containers:
        LOGGER.debug('Erasing the container \'%s\', so a new one can be created', container_name)
        containers[0].remove(force=True)

    container = docker_client.containers.create(image_name, \
        stdin_open=True, tty=True, network_mode='bridge', \
        name=container_name, command='/bin/bash')
    LOGGER.debug('Container \'%s\' created', container_name)
    return container


def wait_for_jenkins(container_name, jenkis_port, timeout):
    client = docker.APIClient()
    container_addr = None

    def get_container_ip():
        nonlocal container_addr
        try:
            container_addr = client.inspect_container(container_name)\
                             ['NetworkSettings']['IPAddress']
        except KeyError:
            return False

        if container_addr:
            return True

        return False

    def ping_jenkins():
        try:
            conn = http.client.HTTPConnection(
                container_addr, jenkis_port, timeout=30)
            conn.request('HEAD', '/fuego/')
            resp = conn.getresponse()
            version = resp.getheader('X-Jenkins')
            status = resp.status
            conn.close()
            LOGGER.debug(
                '  HTTP Response code: \'%d\' - Jenkins Version: \'%s\'',
                status, version)
            if status == http.client.OK and version is not None:
                return True
        except Exception:
            return False

        return False

    def loop_until_timeout(func, timeout, num_tries=5):
        LOGGER.debug('Running %s()', func.__name__)

        for _ in range(num_tries):
            LOGGER.debug('  Try number %s...', _ + 1)
            if func():
                LOGGER.debug('  Success')
                return True
            time.sleep(timeout/num_tries)
        LOGGER.debug('  Failure')

        return False

    if not loop_until_timeout(get_container_ip, timeout):
        LOGGER.error('Could not fetch the container IP address')
        return False

    LOGGER.debug('Trying to reach jenkins at container \'%s\' via' \
                 ' the container\'s IP \'%s\' at port \'%d\'', container_name,
                 container_addr, jenkis_port)
    if not loop_until_timeout(ping_jenkins, timeout):
        LOGGER.error('Could not connect to jenkins')
        return False

    return True

def execute_tests(start_script, container_name, timeout, jenkins_port):
    LOGGER.debug('Starting container \'%s\'', container_name)
    tests_ok = True
    client = pexpect.spawnu('{} {}'.format(start_script, container_name),
                            echo=False,
                            timeout=timeout)
    Command.set_ps1(client)

    if wait_for_jenkins(container_name, jenkins_port, timeout):
        LOGGER.debug('Container ready. Starting tests')
        for cmd in COMMANDS_TO_TEST:
            if not cmd.execute(client):
                tests_ok = False
                break
    else:
        tests_ok = False

    if tests_ok:
        LOGGER.debug('Tests finished.')
    client.terminate(force=True)
    return tests_ok

def main():
    DEFAULT_TIMEOUT = 120
    DEFAULT_IMAGE_NAME = 'fuego-release'
    DEFAULT_CONTAINER_NAME = 'fuego-release-container'
    DEFAULT_INSTALL_SCRIPT = 'install.sh'
    DEFAULT_START_SCRIPT = 'fuego-host-scripts/docker-start-container.sh'
    DEFAULT_JENKINS_PORT = 8080

    parser = argparse.ArgumentParser()
    parser.add_argument('working_dir', help='The working directory', type=str)
    parser.add_argument('-s', '--install-script',
                        help='The script that will be used to install the docker image. Defaults to \'{}\''
                        .format(DEFAULT_INSTALL_SCRIPT),
                        default=DEFAULT_INSTALL_SCRIPT,
                        type=str)
    parser.add_argument('-a', '--start-script',
                        help='The script used to start the container. Defaults to \'{}\''
                        .format(DEFAULT_START_SCRIPT),
                        default=DEFAULT_START_SCRIPT,
                        type=str)
    parser.add_argument('-i', '--image-name', default=DEFAULT_IMAGE_NAME,
                        help='The image name that should be used. Defaults to \'{}\''
                        .format(DEFAULT_IMAGE_NAME), type=str)
    parser.add_argument('-c', '--container-name', default=DEFAULT_CONTAINER_NAME,
                        help='The container name that should be used for the test. Defaults to \'{}\''
                        .format(DEFAULT_CONTAINER_NAME),
                        type=str)
    parser.add_argument('-t', '--timeout', help='The timeout value for commands. Defaults to {}'
                        .format(DEFAULT_TIMEOUT),
                        default=DEFAULT_TIMEOUT, type=int)
    parser.add_argument('-j', '--jenkins-port',
                        help='The port where the jenkins is running on the test container. Defaults to {}'
                        .format(DEFAULT_JENKINS_PORT),
                        default=DEFAULT_JENKINS_PORT, type=int)
    args = parser.parse_args()

    LOGGER.debug('Changing working dir to \'%s\'', args.working_dir)
    os.chdir(args.working_dir)
    container = setup_docker(args.install_script, args.image_name,
                             args.container_name)
    exit_code = 0
    if container is None or not execute_tests(args.start_script, \
      args.container_name, args.timeout, args.jenkins_port):
        exit_code = 1

    container.remove(force=True)
    return exit_code

if __name__ == '__main__':
    sys.exit(main())
