#!/bin/bash

set -e

readonly fuego_release_dir=fuego-release

function test_build {
    if [ -d ${fuego_release_dir} ]; then
        rm -r ${fuego_release_dir}
    fi
    git clone --quiet --depth 1 --single-branch \
        --branch "${FUNCTIONAL_FUEGOTEST_BRANCH}" \
        "${FUNCTIONAL_FUEGOTEST_REPO}" \
        "${fuego_release_dir}"
    cd "${fuego_release_dir}"
    git submodule update --init --recursive
    cd -
}

function test_run {
    sudo -n ${TEST_HOME}/test_run.py "${fuego_release_dir}"
    report "echo ok 1 minimal test on target"
}

function test_processing {
    log_compare "$TESTDIR" "1" "^ok" "p"
}
