#!/bin/bash

set -e

source ./fuego-ro/conf/fuego.conf

readonly internal_jenkins_port="${JENKINS_PORT:-8080}"
readonly fuego_rt_image=fuego-rt
readonly fuego_rt_container=fuego-rt-container
detached_mode_opts=(--interactive --attach)
port_opts=(-p "8080:${internal_jenkins_port}")

usage() {
    echo "usage: ${0} [OPTIONS] up|down"
    grep " .)\\ # ${0}"
    exit 0
}

container_exists() {
    container_name="${1}"
    docker ps -a | grep -q "${container_name}"
    return "${?}"
}

[ "${#}" -eq 0 ] && usage
while getopts "hdcr:b:p:" arg; do
  case "${arg}" in
    d) # Run in detached mode
      detached_mode_opts=()
      ;;
    c) # Start from scratch, rebuilding image
      clean_start="true"
      ;;
    r) # Fuego Repository to be tested (default: Fuego Official Repo)
        fuego_repo_opts=(-e "TEST_REPO=${OPTARG}")
      ;;
    b) # Fuego Branch to be tested (default: Master)
        fuego_branch_opts=(-e "TEST_BRANCH=${OPTARG}")
      ;;
    p) # Fuego (the stable, not the one under test) port mapped on Host (default: 8090)
        port_opts=(-p "${OPTARG}:${internal_jenkins_port}")
      ;;
    h | *) # Display help.
      usage
      exit 0
      ;;
  esac
done
shift "$((OPTIND - 1))"

if [ "${1}" != "up" ] && [ "${1}" != "down" ]; then
    usage
fi

if [ "${1}" = "down" ]; then
    docker stop "${fuego_rt_container}" 2> /dev/null || true
fi

if [ "${1}" = "up" ]; then
    if [ -v clean_start ] || ! container_exists "${container_name}"; then
        if [ "${UID}" == "0" ]; then
            uid=$(id -u "${SUDO_USER}")
            gid=$(id -g "${SUDO_USER}")
        else
            uid="${UID}"
            gid=$(id -g "${USER}")
        fi

        docker build --pull -t "${fuego_rt_image}" .
        docker rm -f "${fuego_rt_container}" 2> /dev/null || true

        docker run -it --name "${fuego_rt_container}" \
            -v /var/run/docker.sock:/var/run/docker.sock \
            -e http_proxy="${http_proxy:-${HTTP_PROXY}}" \
            -e https_proxy="${https_proxy:-${HTTPS_PROXY}}" \
            -e JENKINS_UID="${uid}" \
            -e JENKINS_GID="${gid}" \
            --net=bridge \
            "${port_opts[@]}" \
            "${fuego_repo_opts[@]}" \
            "${fuego_branch_opts[@]}" \
            "${fuego_rt_image}" /fuego-rt-entrypoint.sh /bin/bash

    else
        docker start "${detached_mode_opts[@]}" "${fuego_rt_container}"
    fi
fi
